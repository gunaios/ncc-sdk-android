package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccAbstractNativeConfig extends NccAbstractConfig {

    /**
     * Unit is microseconds as defined by android.hardware.SensorManager.registerListener() API
     *
     * Default is 100Hz, or 100 samples of data per second defined by the inverse of time, 1 / time, e.g., 1 / 10000us = 100Hz
     */
    private int samplingRate = 10000; // microseconds

    NccAbstractNativeConfig() {
    }

    NccAbstractNativeConfig(int samplingRate) {
        this.samplingRate = samplingRate;
    }

    public int getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(int samplingRate) {
        this.samplingRate = samplingRate;
    }
}
