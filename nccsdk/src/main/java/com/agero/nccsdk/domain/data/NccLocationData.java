package com.agero.nccsdk.domain.data;

import android.location.Location;

/**
 * A data class representing a geographical location.
 */

public class NccLocationData extends NccAbstractSensorData {

    private final double latitude;
    private final double longitude;
    private final double altitude;
    private final float course;
    private final float accuracy;
    private final float speed;
    private final double distance;

    /**
     * Creates an instance of NccLocationData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param latitude The latitude in degrees
     * @param longitude The longitude in degrees
     * @param altitude Meters above or below sea level
     * @param course Degrees starting at due north and continuing clockwise around the compass
     * @param accuracy The estimated accuracy of this location, in meters. See {@link Location#getAccuracy()}
     * @param speed The instantaneous speed of the device in the direction of its current heading.
     * @param distance The distance from this location to the previously received location, in meters.
     */
    public NccLocationData(long timestamp, double latitude, double longitude, double altitude, float course, float accuracy, float speed, double distance) {
        super(timestamp);
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.course = course;
        this.accuracy = accuracy;
        this.speed = speed;
        this.distance = distance;
    }

    /**
     * Gets the latitude in degrees
     *
     * @return double of the latitude in degrees
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Gets the longitude in degrees
     *
     * @return double of the longitude in degress
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Gets the altitude above or below sea level in meters
     *
     * @return double of the altitude above or below sea level in meters
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Gets the course measured in degrees starting at due north and continuing clockwise around the compass.
     * Thus, north is 0 degrees, east is 90 degrees, south is 180 degrees, and so on. Course values may not be
     * available on all devices. A negative value indicates that the direction is invalid.
     *
     * @return float of the course measured in degrees starting at due north and continuing clockwise around the compass
     */
    public float getCourse() {
        return course;
    }

    /**
     * Get the estimated accuracy of this location, in meters.
     *
     * See {@link Location#getAccuracy()}
     *
     * @return float of the accuracy in meters
     */
    public float getAccuracy() {
        return accuracy;
    }

    /**
     * Gets the instantaneous speed of the device in the direction of its current heading in meters per second
     *
     * @return float of the instantaneous speed of the device in the direction of its current heading in meters per second
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * Get the distance from this location to the previously received location, in meters.
     *
     * @return double of the distance in meters
     */
    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "NccLocationData{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                ", course=" + course +
                ", accuracy=" + accuracy +
                ", speed=" + speed +
                ", distance=" + distance +
                ", timestamp=" + timestamp +
                '}';
    }
}
