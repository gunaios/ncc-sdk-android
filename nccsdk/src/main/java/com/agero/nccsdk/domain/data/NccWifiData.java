package com.agero.nccsdk.domain.data;

/**
 * Created by james hermida on 12/1/17.
 */

public class NccWifiData extends NccAbstractSensorData {

    private final int type;
    private final String typeName;
    private final String deviceName;
    private final boolean isConnected;
    private final boolean isConnectedOrConnecting;

    public NccWifiData(long timestamp,
                       int type,
                       String typeName,
                       String deviceName,
                       boolean isConnected,
                       boolean isConnectedOrConnecting) {
        super(timestamp);
        this.type = type;
        this.typeName = typeName;
        this.deviceName = deviceName;
        this.isConnected = isConnected;
        this.isConnectedOrConnecting = isConnectedOrConnecting;
    }

    public int getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public boolean isConnectedOrConnecting() {
        return isConnectedOrConnecting;
    }

    @Override
    public String toString() {
        return "NccWifiData{" +
                "type=" + type +
                ", typeName='" + typeName + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", isConnected=" + isConnected +
                ", isConnectedOrConnecting=" + isConnectedOrConnecting +
                ", timestamp=" + timestamp +
                '}';
    }

    public boolean isEqualTo(NccWifiData data) {
        return this.type == data.type
                && this.typeName.equals(data.typeName)
                && (this.deviceName != null && data.deviceName != null) && this.deviceName.equals(data.deviceName)
                && this.isConnected == data.isConnected
                && this.isConnectedOrConnecting == data.isConnectedOrConnecting;
    }
}
