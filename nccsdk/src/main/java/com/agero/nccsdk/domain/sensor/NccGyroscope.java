package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.hardware.SensorEvent;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.domain.data.NccGyroscopeData;

/**
 * Created by james hermida on 8/29/17.
 */

public class NccGyroscope extends NccAbstractNativeSensor {

    /**
     * Creates an instance of NccGyroscope
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     * @throws NccException Exception if there is an error constructing the instance
     */
    public NccGyroscope(Context context, NccConfig config) throws NccException {
        super(context, NccGyroscope.class.getSimpleName(), NccSensorType.GYROSCOPE, config);
    }

    /**
     * See {@link NccAbstractNativeSensor#buildData(SensorEvent, long)}
     */
    @Override
    protected NccAbstractSensorData buildData(SensorEvent sensorEvent, long sensorEventTime) {
        return new NccGyroscopeData(sensorEventTime, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
    }
}
