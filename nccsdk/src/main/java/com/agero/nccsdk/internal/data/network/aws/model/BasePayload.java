package com.agero.nccsdk.internal.data.network.aws.model;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.internal.common.util.AppUtils;
import com.agero.nccsdk.internal.common.util.DeviceUtils;

import java.util.Map;

/**
 * Created by james hermida on 11/13/17.
 */

public class BasePayload {

    private final String userId;
    private final String clientId = AppUtils.getAppPackage(NccSdk.getApplicationContext());
    private final String appVersion = AppUtils.getAppVersion(NccSdk.getApplicationContext());
    private Map<String, Object> metadata;

    private final String os = "A";
    private final String osVersion = DeviceUtils.getOsVersion();
    private final String sdkVersion = NccSdk.getSdkVersion();
    private final String deviceId = DeviceUtils.getDeviceId(NccSdk.getApplicationContext());
    private final String deviceModel = DeviceUtils.getDeviceName();

    BasePayload(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getClientId() {
        return clientId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public String getOs() {
        return os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceModel() {
        return deviceModel;
    }
}
