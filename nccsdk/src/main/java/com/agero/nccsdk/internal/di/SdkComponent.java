package com.agero.nccsdk.internal.di;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.adt.AdtDrivingState;
import com.agero.nccsdk.adt.AdtMonitoringState;
import com.agero.nccsdk.adt.detection.BootCompletedReceiver;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceTransitionsIntentService;
import com.agero.nccsdk.adt.detection.end.AdtEndMonitor;
import com.agero.nccsdk.domain.sensor.ForegroundService;
import com.agero.nccsdk.domain.sensor.NccWifi;
import com.agero.nccsdk.internal.di.module.AdtModule;
import com.agero.nccsdk.internal.di.module.DataModule;
import com.agero.nccsdk.internal.di.module.DomainModule;
import com.agero.nccsdk.internal.di.module.LbtModule;
import com.agero.nccsdk.internal.di.module.NotificationModule;
import com.agero.nccsdk.internal.di.module.SdkModule;
import com.agero.nccsdk.internal.di.module.UbiModule;
import com.agero.nccsdk.lbt.LbtCollectionState;
import com.agero.nccsdk.ubi.UbiCollectionState;
import com.agero.nccsdk.ubi.collection.UbiSessionCollector;
import com.agero.nccsdk.ubi.network.ProcessSessionDataRunnable;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by james hermida on 9/21/17.
 */

@Singleton
@Component(modules = {SdkModule.class, AdtModule.class, DataModule.class, LbtModule.class, DomainModule.class, UbiModule.class, NotificationModule.class})
public interface SdkComponent {

    void inject(NccSdk nccSdk);

    void inject(ForegroundService foregroundService);

    void inject(GeofenceTransitionsIntentService geofenceTransitionsIntentService);

    void inject(UbiCollectionState ubiCollectionState);

    void inject(ProcessSessionDataRunnable processSessionDataRunnable);

    void inject(LbtCollectionState lbtCollectionState);

    void inject(UbiSessionCollector ubiSensorConsumer);

    void inject(AdtEndMonitor adtEndMonitor);

    void inject(AdtMonitoringState adtMonitoringState);

    void inject(AdtDrivingState adtDrivingState);

    void inject(NccWifi nccWifi);

    void inject(BootCompletedReceiver bootCompletedReceiver);
}
