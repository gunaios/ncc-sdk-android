package com.agero.nccsdk.internal.log;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.model.LogPayload;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.common.util.Clock;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.agero.nccsdk.internal.log.Util.getLogLevel;
import static com.agero.nccsdk.internal.common.util.ExceptionUtils.getStacktraceString;

/**
 * Created by james hermida on 11/14/17.
 */

public class ReleaseTree extends Timber.Tree {

    private final LogConfig config;
    private final DataManager dataManager;
    private final KinesisClient kinesisClient;
    private final Clock clock;

    private final Gson gson;

    public ReleaseTree(LogConfig logConfig, DataManager dataManager, KinesisClient kinesisClient, Clock clock) {
        this.config = logConfig;
        this.dataManager = dataManager;
        this.kinesisClient = kinesisClient;
        this.clock = clock;
        this.gson = new Gson();
    }

    @Override
    protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {
        // Report logs to Kinesis Firehose based on config
        if (getLogLevel(priority) >= config.getMinLogLevel()) {
            long timeUtc = clock.currentTimeMillis();
            String timeLocal = clock.getLocalTimestamp(timeUtc);

            LogPayload logPayload = new LogPayload(getLogLevel(priority), dataManager.getUserId(), message);
            logPayload.setTimeUtc(timeUtc);
            logPayload.setTimeLocal(timeLocal);
            if (dataManager.getTrackingContext() != null) {
                logPayload.setMetadata(dataManager.getTrackingContext().getData());
            }

            if (getLogLevel(priority) == LogLevel.ERROR.getId() && t != null) {
                logPayload.setStacktrace(getStacktraceString(t));
            }

            kinesisClient.save(gson.toJson(logPayload), BuildConfig.KINESIS_STREAM_LOGS);
        }
    }

    // TODO handle config changes
}
