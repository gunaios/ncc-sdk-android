package com.agero.nccsdk.internal.log;

import static android.util.Log.ASSERT;
import static android.util.Log.DEBUG;
import static android.util.Log.ERROR;
import static android.util.Log.INFO;
import static android.util.Log.VERBOSE;
import static android.util.Log.WARN;

/**
 * Created by james hermida on 11/20/17.
 */

class Util {

    static int getLogLevel(final int nativePriority) {
        switch (nativePriority) {
            case VERBOSE:
                return LogLevel.VERBOSE.getId();
            case DEBUG:
                return LogLevel.DEBUG.getId();
            case INFO:
                return LogLevel.INFO.getId();
            case WARN:
                return LogLevel.WARN.getId();
            case ERROR:
                return LogLevel.ERROR.getId();
            case ASSERT:
                return LogLevel.ASSERT.getId();
            default:
                return -1; // UNKNOWN
        }
    }

}
