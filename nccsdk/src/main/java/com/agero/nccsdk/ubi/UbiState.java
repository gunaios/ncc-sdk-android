package com.agero.nccsdk.ubi;

import com.agero.nccsdk.internal.common.statemachine.State;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;

/**
 * Created by james hermida on 9/6/17.
 */

public interface UbiState extends State {

    void startCollecting(StateMachine<UbiState> machine);
    void stopCollecting(StateMachine<UbiState> machine);

}
