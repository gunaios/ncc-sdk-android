package com.agero.nccsdk.ubi.collection;

import android.content.Context;

/**
 * Created by james hermida on 11/29/17.
 */

public abstract class AbstractSensorCollector implements SensorCollector {

    protected final Context applicationContext;
    protected boolean isStarted = false;

    protected AbstractSensorCollector(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }
}
