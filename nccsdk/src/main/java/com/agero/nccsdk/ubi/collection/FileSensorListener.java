package com.agero.nccsdk.ubi.collection;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.internal.common.io.FileWriterWrapper;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.ubi.collection.mapper.SensorDataMapper;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class FileSensorListener implements NccSensorListener {

    private final FileWriterWrapper sensorWriter;
    private final SensorDataMapper sensorMapper;

    FileSensorListener(FileWriterWrapper sensorWriter, SensorDataMapper sensorMapper) {
        this.sensorWriter = sensorWriter;
        this.sensorMapper = sensorMapper;
    }

    @Override
    public void onDataChanged(NccSensorType sensorType, NccSensorData data) {
        try {
            sensorWriter.appendLine(sensorMapper.map(data));
        } catch (IOException ioe) {
            File file = sensorWriter.getFile();
            Timber.e(ioe, "Exception writing data for sensor %s; %s",
                    sensorType.getName(),
                    (file != null
                            ? String.format(Locale.getDefault(), "x:%s r:%s w:%s", file.canExecute(), file.canRead(), file.canWrite())
                            : "file null"));
        }
    }

    public FileWriterWrapper getSensorWriter() {
        return sensorWriter;
    }
}
