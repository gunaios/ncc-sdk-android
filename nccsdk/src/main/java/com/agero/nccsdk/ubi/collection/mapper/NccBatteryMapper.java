package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccBatteryData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccBatteryMapper extends AbstractSensorDataMapper {

    static final String IS_NOT_CHARGING = "0";
    static final String IS_CHARGING = "1";

    @Override
    public String map(NccSensorData data) {
        NccBatteryData bd = (NccBatteryData) data;
        clearStringBuilder();
        sb.append("B,");                                                                // Code
        sb.append(bd.getLevel()); sb.append(",");                                       // Level
        sb.append((bd.isCharging() ? IS_CHARGING : IS_NOT_CHARGING)); sb.append(",");   // Power
        sb.append("NA,NA,NA,NA,NA,NA,NA,");
        sb.append(bd.getTimestamp()); sb.append(",");                                   // UTCTime
        sb.append(getReadableTimestamp(bd.getTimestamp()));                             // Timestamp
        return sb.toString();
    }
}
