package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccBatteryData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccBatteryMapperTest {

    private AbstractSensorDataMapper mapper = new NccBatteryMapper();

    private long timeUtc = 1315314000000L;
    private String formattedDate = mapper.getReadableTimestamp(timeUtc);
    private int level = 90;

    @Test
    public void map_charging() throws Exception {
        boolean isCharging = true;
        NccSensorData data = new NccBatteryData(timeUtc, level, isCharging);

        String expected = "B," + level + "," + NccBatteryMapper.IS_CHARGING + ",NA,NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }

    @Test
    public void map_not_charging() throws Exception {
        boolean isCharging = false;
        NccSensorData data = new NccBatteryData(timeUtc, level, isCharging);

        String expected = "B," + level + "," + NccBatteryMapper.IS_NOT_CHARGING + ",NA,NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
